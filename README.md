Tomato-Tomato
=============

A Hugo theme built with the Semantic UI CSS Framework

## About

This theme offers a simple [Fixed Menu]("https://semantic-ui.com/examples/fixed.html) layout, including a slim footer with menu. It
uses the pre-compiled v2.3.3 [semantic-ui-css](https://github.com/Semantic-Org/Semantic-UI-CSS) library and it's default theme.
The Sematic UI library is [MIT Licensed](https://github.com/Semantic-Org/Semantic-UI-CSS/blob/master/LICENSE).


`index.html` is configured to present a *Site Logo* front and center, along with the *Logo Name* and *Logo Slogan* underneath it.
It will dump any content in `_index.md` after that, as usual.

An inverted version of the *Site Logo* is also in the top *Nav Menu*, as well as the bottom *Footer Menu*

## Configuration

Simply override any of the parameters in your `config.toml` file, or add the appropriate files in your `/static` folder.

```
[params]

  accentLogo = "/images/logo-accent.png"
  logo = "/images/logo.png"
  lame = "Logo + Name"
  slogo = "A Slogan for your Logo"
```

Note: The *Nav/Footer Menu*s are black, so your `accentLogo` should contrast appropriately!

## Nav Menu

To add a page link to the top menu, simple add it to the `tomato_tomato` menu in the front matter. You can adjust the
order in the menu by selecting an appropriate `weight`. Example:

```
+++
title = "About"

menu = "tomato_tomato"
weight = 1
+++
```

## Footer Menu

To add a page link to the footer menu, simple add it to the `tomato_footer` menu in the front matter. You can adjust the
order in the menu by selecting an appropriate `weight`. Example:

```
+++
title = "Privacy Policy"

menu = "tomato_footer"
weight = 1
+++
```